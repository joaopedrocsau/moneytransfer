# moneytransferapi

To run the application:
mvn install -Dmaven.test.skip=true
mvn exec:java

To run the tests:
mvn test

server runs on port 7091

## WEBAPI
API developed to support money transfer between accounts.

These can be used alone like this

| resource      | description                       |
|:--------------|:----------------------------------|
| `/moneytransfer/getAccount`      | returns a list of all existent accounts
| `/moneytransfer/getAccountById/:id`    | returns the account details of the account with the given id
| `/moneytransfer/createAccount` | returns the details of the created account |
| `/moneytransfer/deleteAccount/:id`      | returns sucess if the operation succeeds, error otherwise |
| `/moneytransfer/getTransactions`  | returns a list of all existent transactions |
| `/moneytransfer/getTransactionById/:id` | returns the transaction details of the transaction with the given id |
| `/moneytransfer/createTransaction` | returns the details of the created transaction |
| `/moneytransfer/executeTransaction` | creates and executes the transaction with the given request |
| `/moneytransfer/executeTransactionById/:id` | executes the transaction with the given id |
| `/moneytransfer/deleteTransaction/:id` | returns sucess if the operation succeeds, error otherwise |


## Account Request json example
   {
    "balance": 200, 
     "user": "1123"
   }
   
## Transaction Request json example
{
    "amount": 50, 
    "source": "1012",
    "destination": "1013"
}
