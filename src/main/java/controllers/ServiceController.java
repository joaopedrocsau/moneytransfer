package controllers;

import handlers.AccountHandler;
import handlers.TransactionHandler;
import org.apache.log4j.Logger;

import static spark.Spark.*;

public class ServiceController {
    public static void main(String[] args) {
        int maxThreads = 8;
        int minThreads = 2;
        int timeOutMillis = 30000;
        threadPool(maxThreads, minThreads, timeOutMillis);
        port(7091);

        get("/moneytransfer/getAccount", AccountHandler::getAccount);
        get("/moneytransfer/getAccountById/:id", AccountHandler::getAccountById);
        post("/moneytransfer/createAccount", AccountHandler::createAccount);
        delete("/moneytransfer/deleteAccount/:id",  AccountHandler::deleteAccount);

        get("moneytransfer/getTransactions", TransactionHandler::getTransactions);
        get("/moneytransfer/getTransactionById/:id", TransactionHandler::getTransactionById);
        post("moneytransfer/createTransaction", TransactionHandler::createTransaction);
        post("moneytransfer/executeTransaction", TransactionHandler::executeTransaction);
        post("moneytransfer/executeTransactionById/:id", TransactionHandler::executeTransactionById);
        delete("/moneytransfer/deleteTransaction/:id",  TransactionHandler::deleteTransaction);
    }
}
