package dao;

import types.Account;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

public class AccountDAO implements IAccountDAO {
    private HashMap<UUID, Account> accountMap;

    public AccountDAO() {
        this.accountMap = new HashMap<>();
    }

    public Account create(float initialBalance, String user) {
        UUID newAccountId = UUID.randomUUID();
        while (accountMap.keySet().contains(newAccountId)) {
            newAccountId = UUID.randomUUID();
        }

        Account newAccount = new Account(newAccountId, initialBalance, user);
        this.accountMap.put(newAccount.getId(), newAccount);
        return newAccount;
    }

    public Account getAccount(String id) {
        return accountMap.get(UUID.fromString(id));
    }

    public Collection<Account> getAccounts() {
        return accountMap.values();
    }

    public void deleteAccount(String id) {
        accountMap.remove(UUID.fromString(id));
    }

    public boolean isValidAccount(String id){
        return this.getAccount(id).isValidAccount();
    }

}
