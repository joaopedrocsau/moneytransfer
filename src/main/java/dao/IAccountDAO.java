package dao;

import types.Account;

import java.util.Collection;

public interface IAccountDAO {
    public Account create(float initialBalance, String user);

    public Account getAccount(String id);

    public Collection<Account> getAccounts();

    public void deleteAccount(String id);

    public boolean isValidAccount(String id);
}
