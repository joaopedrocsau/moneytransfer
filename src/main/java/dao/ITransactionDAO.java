package dao;

import types.Account;
import types.Transaction;

import java.util.Collection;

public interface ITransactionDAO {
    public Transaction create(float amount, Account source, Account destination);

    public Transaction getTransaction(String id);

    public Collection<Transaction> getTransactions();

    public void deleteTransaction(String id);
}
