package dao;

import types.Account;
import types.Transaction;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

public class TransactionDAO implements ITransactionDAO {
    private HashMap<UUID, Transaction> transactionMap;

    public TransactionDAO() {
        this.transactionMap = new HashMap<>();
    }

    @Override
    public Transaction create(float amount, Account source, Account destination) {
        UUID newTransactionId = UUID.randomUUID();
        while (transactionMap.keySet().contains(newTransactionId)) {
            newTransactionId = UUID.randomUUID();
        }

        Transaction newTransaction = new Transaction(newTransactionId, amount, source, destination);
        this.transactionMap.put(newTransaction.getId(), newTransaction);
        return newTransaction;
    }

    public Transaction getTransaction(String id) {
        return transactionMap.get(UUID.fromString(id));
    }

    public Collection<Transaction> getTransactions() {
        return transactionMap.values();
    }

    public void deleteTransaction(String id) {
        transactionMap.remove(UUID.fromString(id));
    }
}
