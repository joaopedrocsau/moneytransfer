package handlers;

import com.google.gson.Gson;
import dao.AccountDAO;
import dao.IAccountDAO;
import service.IAccountService;
import service.AccountService;
import spark.Request;
import spark.Response;
import types.Account;
import types.response.StandardResponse;
import types.response.StatusResponse;
import types.request.AccountRequest;
import org.apache.log4j.Logger;

public class AccountHandler {
    private static final Logger logger = Logger.getLogger(AccountHandler.class);
    private static final IAccountDAO accountDAO = new AccountDAO();
    private static final IAccountService accountService = new AccountService(accountDAO);

    public static IAccountService getAccountService() {
        return accountService;
    }

    public static String createAccount(Request request, Response response) {
        try {
            response.type("application/json");

            AccountRequest account = new Gson().fromJson(request.body(), AccountRequest.class);
            Account newAccount = accountService.createAccount(account.getBalance(), account.getUser());

            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                    new Gson().toJsonTree(newAccount, Account.class)));
        } catch (Exception e) {
            logger.error("createAccount: " + e.getMessage());
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String getAccount(Request request, Response response) {
        try {
            response.type("application/json");

            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                    new Gson().toJsonTree(accountService.getAccounts())));
        } catch (Exception e) {
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String getAccountById(Request request, Response response) {
        try {
            response.type("application/json");
            //TODO ADD INPUT VALIDATION ALSO
            if (accountService.getAccount(request.params(":id")) == null) {
                logger.error("getAccountById: invalid id");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
            } else {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(accountService.getAccount(request.params(":id")))));
            }
        } catch (Exception e) {
            logger.error("getAccountById: " + e.getMessage());
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String deleteAccount(Request request, Response response) {
        try {
            response.type("application/json");

            accountService.deleteAccount(request.params(":id"));
            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                    "Account with id: " + request.params(":id")));
        } catch (Exception e) {
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }


}
