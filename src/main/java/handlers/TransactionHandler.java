package handlers;

import com.google.gson.Gson;
import dao.ITransactionDAO;
import dao.TransactionDAO;
import org.apache.log4j.Logger;
import service.IAccountService;
import service.ITransactionService;
import service.TransactionService;
import spark.Request;
import spark.Response;
import types.Account;
import types.Transaction;
import types.request.TransactionRequest;
import types.response.StandardResponse;
import types.response.StatusResponse;


public class TransactionHandler {
    private static final Logger logger = Logger.getLogger(TransactionHandler.class);
    private static final AccountHandler accountHandler = new AccountHandler();
    private static final IAccountService accountService = accountHandler.getAccountService();
    private static final ITransactionDAO transactionDAO = new TransactionDAO();
    private static final ITransactionService transactionService = new TransactionService(transactionDAO);

    public static String createTransaction(Request request, Response response) {
        try {
            response.type("application/json");

            TransactionRequest transactionRequest = new Gson().fromJson(request.body(), TransactionRequest.class);
            Account source = accountService.getAccount(transactionRequest.getSource());
            Account destination = accountService.getAccount(transactionRequest.getDestination());
            float amount = transactionRequest.getAmount();

            if (source == null) {
                logger.error("createTransaction: Invalid Source.");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Invalid Source."));
            } else if (destination == null) {
                logger.error("createTransaction: Invalid Destination.");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Invalid Destination."));
            } else if (amount <= 0) {
                logger.error("createTransaction: Invalid Amount.");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Invalid amount."));
            } else {
                Transaction newTransaction = transactionService.createTransaction(amount, source, destination);
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(newTransaction)));
            }
        } catch (Exception e) {
            logger.error("createTransaction: "+e.getMessage());
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String executeTransactionById(Request request, Response response) {
        try {
            response.type("application/json");

            String id = request.params(":id");
            if (id == null || id.isEmpty()) {
                logger.error("executeTransactionById: empty id");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
            } else if (transactionService.getTransaction(id) == null) {
                logger.error("executeTransactionById: invalid id");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
            } else {
                boolean transactionResult = transactionService.executeTransaction(id);
                if(!transactionResult) {
                    logger.error("executeTransactionById: failed");
                    return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
                }
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(transactionService.getTransaction(id))));
            }
        } catch (Exception e) {
            logger.error("executeTransactionById: "+e.getMessage());
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String executeTransaction(Request request, Response response){
        try {
            response.type("application/json");

            TransactionRequest transactionRequest = new Gson().fromJson(request.body(), TransactionRequest.class);
            Account source = accountService.getAccount(transactionRequest.getSource());
            Account destination = accountService.getAccount(transactionRequest.getDestination());
            float amount = transactionRequest.getAmount();

            if (source == null) {
                logger.error("createTransaction: Invalid Source.");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Invalid Source."));
            } else if (destination == null) {
                logger.error("createTransaction: Invalid Destination.");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Invalid Destination."));
            } else if (amount <= 0) {
                logger.error("createTransaction: Invalid Amount.");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Invalid amount."));
            } else {
                Transaction newTransaction = transactionService.createTransaction(amount, source, destination);
                String newTransactionId = newTransaction.getId().toString();
                if (newTransactionId == null || newTransactionId.isEmpty()){
                    logger.error("executeTransactionById: failed");
                    return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
                }
                boolean transactionResult = transactionService.executeTransaction(newTransactionId);
                if(!transactionResult) {
                    logger.error("executeTransactionById: failed");
                    return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
                }
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(newTransaction)));
            }
        } catch (Exception e) {
            logger.error("createTransaction: "+e.getMessage());
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String getTransactions(Request request, Response response){
        try {
            response.type("application/json");

            return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                    new Gson().toJsonTree(transactionService.getTransactions())));
        } catch (Exception e) {
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String getTransactionById(Request request, Response response){
        try {
            response.type("application/json");
            String id = request.params(":id");
            if (id == null || id.isEmpty()) {
                logger.error("executeTransactionById: empty id");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
            } else if (transactionService.getTransaction(id) == null) {
                logger.error("executeTransactionById: invalid id");
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
            } else {
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(transactionService.getTransaction(id))));
            }
        } catch (Exception e) {
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }

    public static String deleteTransaction(Request request, Response response){
        try {
            response.type("application/json");
            String id = request.params(":id");
            if(id.isEmpty() || id == null) {
                return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "Empty Id."));
            } else {
                transactionService.deleteTransaction(id);
                return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS,
                        new Gson().toJsonTree(transactionService.getTransactions())));
            }
        } catch (Exception e) {
            return new Gson().toJson(new StandardResponse(StatusResponse.ERROR));
        }
    }
}
