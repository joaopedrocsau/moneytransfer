package service;

import dao.IAccountDAO;
import types.Account;

import java.util.Collection;

public class AccountService implements IAccountService {
    private IAccountDAO accountDAO;

    public AccountService(IAccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public Account createAccount(float balance, String user) {
        return accountDAO.create(balance, user);
    }

    @Override
    public Account getAccount(String id) {
        return accountDAO.getAccount(id);
    }

    @Override
    public Collection<Account> getAccounts() {
        return accountDAO.getAccounts();
    }

    @Override
    public void deleteAccount(String id) {
        accountDAO.deleteAccount(id);
    }

    @Override
    public boolean isValidAccount(String id) {
        return accountDAO.isValidAccount(id);
    }
}
