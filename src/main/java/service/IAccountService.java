package service;

import types.Account;

import java.util.Collection;
import java.util.UUID;

public interface IAccountService {

    public Account createAccount (float balance, String user);

    public Account getAccount (String id);

    public Collection<Account> getAccounts ();

    public void deleteAccount (String id);

    public boolean isValidAccount (String id);
}