package service;

import types.Account;
import types.Transaction;
import types.exception.TransactionException;

import java.util.Collection;
import java.util.UUID;

public interface ITransactionService {

    public Transaction createTransaction(float amount, Account source, Account destination) throws TransactionException;

    public Transaction getTransaction (String id);

    public Collection<Transaction> getTransactions ();

    public boolean executeTransaction (String id);

    public void deleteTransaction (String id);
}