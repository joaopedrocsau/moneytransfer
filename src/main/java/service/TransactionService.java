package service;

import dao.ITransactionDAO;
import types.Account;
import types.Transaction;
import types.exception.TransactionException;

import java.util.Collection;

public class TransactionService implements ITransactionService {
    //TODO add logger
    private ITransactionDAO transactionDAO;

    public TransactionService(ITransactionDAO transactionDAO) {
        this.transactionDAO = transactionDAO;
    }

    @Override
    public Transaction createTransaction(float amount, Account source, Account destination) throws TransactionException{
            if(!source.isValidAccount()) {
                throw new TransactionException("Account with id:" + source.getId().toString() +  "is not valid.");
            } else if(!destination.isValidAccount()) {
                throw new TransactionException("Account with id:" + source.getId().toString() +  "is not valid.");
            } else {
                Transaction newTransaction = this.transactionDAO.create(amount, source, destination);
                return newTransaction;
            }
        }

    @Override
    public Transaction getTransaction(String id) {
        return transactionDAO.getTransaction(id);
    }

    @Override
    public Collection<Transaction> getTransactions() {
        return transactionDAO.getTransactions();
    }

    @Override
    public boolean executeTransaction(String id) {
        return transactionDAO.getTransaction(id).execute();
    }

    @Override
    public void deleteTransaction(String id){
        transactionDAO.deleteTransaction(id);
    }
}
