package types;

import types.exception.AccountException;

import java.util.UUID;

public class Account {
    private UUID id;
    private float balance;
    private String status;
    private String user;

    public Account(UUID id, float balance, String user) {
        this.id = id;
        this.balance = balance;
        this.status = "valid";
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public UUID getId() {
        return id;
    }

    public float getBalance() {
        return balance;
    }

    public String getUser() {
        return user;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setBalance(float balance) throws AccountException {
        if (balance < 0) {
            throw new AccountException("Operation would result in negative balance.");
        } else {
            this.balance = balance;
        }
    }

    public void addBalance(float amount) {
        this.balance += amount;
    }

    public void subBalance(float amount) throws AccountException {
        if (amount > 0 && (this.balance - amount < 0)) {
            throw new AccountException("Operation would result in negative balance.");
        } else if (amount < 0 && (this.balance + amount < 0)){
            throw new AccountException("Operation would result in negative balance.");
        }
        else {
            this.balance -= amount;
        }
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isValidAccount() {
        return status == "valid";
    }
}