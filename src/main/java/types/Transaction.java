package types;

import java.util.UUID;

public class Transaction {
    private UUID id;
    private String status;
    private float amount;
    private Account source;
    private Account destination;

    public Transaction(UUID id, float amount, Account source, Account destination) {
        this.id = id;
        this.source = source;
        this.status = "pending";
        this.destination = destination;
        this.amount = amount;
    }

    public boolean execute() {
        try {
            if (!source.isValidAccount() || !destination.isValidAccount()) {
                this.setStatus("rejected");
                return false;
            } else if (source.getBalance() < this.getAmount()) {
                this.setStatus("rejected");
                return false;
            } else {
                source.subBalance(amount);
                destination.addBalance(amount);
                this.setStatus("executed");
                return true;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public UUID getId() {
        return id;
    }

    public Account getSource() {
        return source;
    }

    public void setSource(Account source) {
        this.source = source;
    }

    public Account getDestination() {
        return destination;
    }

    public void setDestination(Account destination) {
        this.destination = destination;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getAmount() {
        return amount;
    }
}
