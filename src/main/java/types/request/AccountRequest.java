package types.request;

public class AccountRequest {
    float balance;
    String user;

    public AccountRequest(float balance, String user) {
        this.balance = balance;
        this.user = user;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
