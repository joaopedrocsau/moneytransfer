package types.request;

public class TransactionRequest {
    String source;
    String destination;
    float amount;

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public float getAmount() {
        return amount;
    }
}
