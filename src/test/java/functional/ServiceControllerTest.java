package functional;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

public class ServiceControllerTest extends FunctionalTest {

    @Test
    public void createAccount() {
        Map<String, Object> account = new HashMap<>();
        account.put("balance", 200.0f);
        account.put("user", "12345");

        given()
                .contentType("application/json")
                .body(account)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200);
    }

    @Test
    public void getAccountAfterCreation() {
        Map<String, Object> account = new HashMap<>();
        account.put("balance", 200.0f);
        account.put("user", "12345");

        String newAccountId = given()
                .contentType("application/json")
                .body(account)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");;

        given().when().get("/getAccountById/" + newAccountId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", equalTo(newAccountId))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200);
    }

    @Test
    public void getAccounts() {
        Map<String, Object> accountA = new HashMap<>();
        accountA.put("balance", 200.0f);
        accountA.put("user", "12345");

        Map<String, Object> accountB = new HashMap<>();
        accountB.put("balance", 150.0f);
        accountB.put("user", "12346");

        String firstNewAccount = given()
                .contentType("application/json")
                .body(accountA)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        String secondNewAccount = given()
                .contentType("application/json")
                .body(accountB)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12346"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccount").then()
                .body("status", equalTo("SUCCESS"))
                .statusCode(200);
    }

    @Test
    public void deleteAccountAfterCreation(){
        Map<String, Object> account = new HashMap<>();
        account.put("balance", 200.0f);
        account.put("user", "12345");

        String newAccountId = given()
                .contentType("application/json")
                .body(account)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + newAccountId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", equalTo(newAccountId))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200);

        given().when().delete("/deleteAccount/" + newAccountId).then()
                .body("status", equalTo("SUCCESS"))
                .statusCode(200);

        given().when().get("/getAccountById/" + newAccountId).then()
                .body("status", equalTo("ERROR"))
                .statusCode(200);

    }

    @Test
    public void createAndGetTransaction() {
        Map<String, Object> accountA = new HashMap<>();
        accountA.put("balance", 200.0f);
        accountA.put("user", "12345");

        Map<String, Object> accountB = new HashMap<>();
        accountB.put("balance", 150.0f);
        accountB.put("user", "12346");

        String accountAId = given()
                .contentType("application/json")
                .body(accountA)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        String accountBId = given()
                .contentType("application/json")
                .body(accountB)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12346"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        Map<String, Object> transaction = new HashMap<>();
        transaction.put("amount", 50.0f);
        transaction.put("source", accountAId);
        transaction.put("destination", accountBId);

        String transactionId = given()
                .contentType("application/json")
                .body(transaction)
                .when().post("/createTransaction")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("pending"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given()
                .contentType("application/json")
                .body(transaction)
                .when().get("/getTransactionById/" + transactionId)
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", equalTo(transactionId))
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("pending"))
                .statusCode(200);
    }

    @Test
    public void createAndExecuteTransactionById() {
        Map<String, Object> accountA = new HashMap<>();
        accountA.put("balance", 200.0f);
        accountA.put("user", "12345");

        Map<String, Object> accountB = new HashMap<>();
        accountB.put("balance", 150.0f);
        accountB.put("user", "12346");

        String accountAId = given()
                .contentType("application/json")
                .body(accountA)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        String accountBId = given()
                .contentType("application/json")
                .body(accountB)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12346"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        Map<String, Object> transaction = new HashMap<>();
        transaction.put("amount", 50.0f);
        transaction.put("source", accountAId);
        transaction.put("destination", accountBId);

        String transactionId = given()
                .contentType("application/json")
                .body(transaction)
                .when().post("/createTransaction")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("pending"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given()
                .contentType("application/json")
                .body(transaction)
                .when().post("/executeTransactionById/" + transactionId)
                .then()
                .body("status", equalTo("SUCCESS"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given()
                .contentType("application/json")
                .body(transaction)
                .when().get("/getTransactionById/" + transactionId)
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", equalTo(transactionId))
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("executed"))
                .statusCode(200);
    }

    @Test
    public void executeTransaction(){
        Map<String, Object> accountA = new HashMap<>();
        accountA.put("balance", 200.0f);
        accountA.put("user", "12345");

        Map<String, Object> accountB = new HashMap<>();
        accountB.put("balance", 150.0f);
        accountB.put("user", "12346");

        String accountAId = given()
                .contentType("application/json")
                .body(accountA)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        String accountBId = given()
                .contentType("application/json")
                .body(accountB)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12346"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        Map<String, Object> transaction = new HashMap<>();
        transaction.put("amount", 50.0f);
        transaction.put("source", accountAId);
        transaction.put("destination", accountBId);

        String transactionId = given()
                .contentType("application/json")
                .body(transaction)
                .when().post("/executeTransaction")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("executed"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given()
                .contentType("application/json")
                .body(transaction)
                .when().get("/getTransactionById/" + transactionId)
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", equalTo(transactionId))
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("executed"))
                .statusCode(200);
    }

    @Test
    public void getTransactions(){
        Map<String, Object> accountA = new HashMap<>();
        accountA.put("balance", 200.0f);
        accountA.put("user", "12345");

        Map<String, Object> accountB = new HashMap<>();
        accountB.put("balance", 150.0f);
        accountB.put("user", "12346");

        String accountAId = given()
                .contentType("application/json")
                .body(accountA)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        String accountBId = given()
                .contentType("application/json")
                .body(accountB)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12346"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        Map<String, Object> transaction = new HashMap<>();
        transaction.put("amount", 50.0f);
        transaction.put("source", accountAId);
        transaction.put("destination", accountBId);

        String transactionId = given()
                .contentType("application/json")
                .body(transaction)
                .when().post("/createTransaction")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("pending"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given()
                .contentType("application/json")
                .body(transaction)
                .when().get("/getTransactions")
                .then()
                .body("status", equalTo("SUCCESS"))
                .statusCode(200);
    }

    @Test
    public void deleteTransactionAfterCreation(){
        Map<String, Object> accountA = new HashMap<>();
        accountA.put("balance", 200.0f);
        accountA.put("user", "12345");

        Map<String, Object> accountB = new HashMap<>();
        accountB.put("balance", 150.0f);
        accountB.put("user", "12346");

        String accountAId = given()
                .contentType("application/json")
                .body(accountA)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12345"))
                .statusCode(200)
                .extract()
                .path("data.id");

        String accountBId = given()
                .contentType("application/json")
                .body(accountB)
                .when().post("/createAccount")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .body("data.user", equalTo("12346"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given().when().get("/getAccountById/" + accountAId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(200.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        given().when().get("/getAccountById/" + accountBId).then()
                .body("status", equalTo("SUCCESS"))
                .body("data.balance", equalTo(150.0f))
                .body("data.status", equalTo("valid"))
                .statusCode(200);

        Map<String, Object> transaction = new HashMap<>();
        transaction.put("amount", 50.0f);
        transaction.put("source", accountAId);
        transaction.put("destination", accountBId);

        String transactionId = given()
                .contentType("application/json")
                .body(transaction)
                .when().post("/createTransaction")
                .then()
                .body("status", equalTo("SUCCESS"))
                .body("data.id", notNullValue())
                .body("data.amount", equalTo(50.0f))
                .body("data.status", equalTo("pending"))
                .statusCode(200)
                .extract()
                .path("data.id");

        given()
                .contentType("application/json")
                .body(transaction)
                .when().delete("/deleteTransaction/" + transactionId)
                .then()
                .body("status", equalTo("SUCCESS"))
                .statusCode(200);

        given()
                .contentType("application/json")
                .body(transaction)
                .when().get("/getTransactionById/" + transactionId)
                .then()
                .body("status", equalTo("ERROR"))
                .statusCode(200);
    }
}