package unit;

import types.Account;
import org.junit.Assert;
import org.junit.Test;
import types.exception.AccountException;

import java.util.UUID;

public class AccountTypeTest {
    @Test
    public void createAccount(){
        UUID uuid = UUID.randomUUID();
        Account newAccount = new Account(uuid, 0.0f, "johndoe");
        Assert.assertEquals(uuid, newAccount.getId());
        Assert.assertEquals("valid", newAccount.getStatus());
        Assert.assertEquals(0.0f,newAccount.getBalance(), 0.0f);
        Assert.assertEquals("johndoe", newAccount.getUser());
    }

    @Test
    public void changeBalance() throws AccountException {
        UUID uuid = UUID.randomUUID();
        Account newAccount = new Account(uuid, 0.0f, "johndoe");
        Assert.assertEquals(0.0f,newAccount.getBalance(), 0.0f);
        newAccount.setBalance(2.0f);
        Assert.assertEquals(2.0f,newAccount.getBalance(), 0.0f);
    }

    @Test
    public void changeUser(){
        UUID uuid = UUID.randomUUID();
        Account newAccount = new Account(uuid, 0.0f, "johndoe");
        Assert.assertEquals("johndoe", newAccount.getUser());
        newAccount.setUser("charlesdoe");
        Assert.assertEquals("charlesdoe", newAccount.getUser());
    }

    @Test
    public void changeId(){
        UUID uuid = UUID.randomUUID();
        Account newAccount = new Account(uuid, 0.0f, "johndoe");
        Assert.assertEquals(uuid, newAccount.getId());
        UUID newUuid = UUID.randomUUID();
        newAccount.setId(newUuid);
        Assert.assertEquals(newUuid, newAccount.getId());
    }

    @Test
    public void changeStatus(){
        UUID uuid = UUID.randomUUID();
        Account newAccount = new Account(uuid, 0.0f, "johndoe");
        Assert.assertEquals("valid", newAccount.getStatus());
        newAccount.setStatus("pending");
        Assert.assertEquals("pending", newAccount.getStatus());
    }
}
