package unit;

import dao.AccountDAO;
import dao.IAccountDAO;
import dao.ITransactionDAO;
import dao.TransactionDAO;
import service.AccountService;
import service.IAccountService;
import service.ITransactionService;
import service.TransactionService;
import types.Account;
import types.Transaction;
import org.junit.Assert;
import org.junit.Test;
import types.exception.TransactionException;

import java.util.UUID;

public class TransactionServiceTest {
    @Test
    public void createTransaction() throws TransactionException {
        IAccountDAO accountDAO = new AccountDAO();
        IAccountService accountService = new AccountService(accountDAO);
        Account source = accountService.createAccount(0.0f, "johndoe");
        Account destination = accountService.createAccount(0.0f, "janedoe");

        ITransactionDAO transactionDAO = new TransactionDAO();
        ITransactionService transactionService = new TransactionService(transactionDAO);
        Transaction newTransaction = transactionService.createTransaction(10.0f, source, destination);

        Assert.assertNotNull(newTransaction.getId());
        Assert.assertEquals("pending", newTransaction.getStatus());
        Assert.assertEquals(10.0f,newTransaction.getAmount(), 0.0f);
        Assert.assertEquals(source, newTransaction.getSource());
        Assert.assertEquals(destination, newTransaction.getDestination());
    }

    @Test
    public void successfulTransaction() throws TransactionException {
        IAccountDAO accountDAO = new AccountDAO();
        AccountService accountService = new AccountService(accountDAO);
        Account source = accountService.createAccount(20.0f, "johndoe");
        Account destination = accountService.createAccount(0.0f, "janedoe");


        ITransactionDAO transactionDAO = new TransactionDAO();
        ITransactionService transactionService = new TransactionService(transactionDAO);
        Transaction newTransaction = transactionService.createTransaction(10.0f, source, destination);
        boolean executionResult = newTransaction.execute();

        Assert.assertNotNull(newTransaction.getId());
        Assert.assertEquals("executed", newTransaction.getStatus());
        Assert.assertEquals(10.0f,newTransaction.getAmount(), 0.0f);
        Assert.assertEquals(source, newTransaction.getSource());
        Assert.assertEquals(destination, newTransaction.getDestination());
        Assert.assertTrue(executionResult);

        Assert.assertEquals(10.0f, source.getBalance(), 0.0f);
        Assert.assertEquals(10.0f, destination.getBalance(), 0.0f);
    }

    @Test
    public void notEnoughBalanceTransaction() throws TransactionException {
        IAccountDAO accountDAO = new AccountDAO();
        AccountService accountService = new AccountService(accountDAO);
        Account source = accountService.createAccount(0.0f, "johndoe");
        Account destination = accountService.createAccount(0.0f, "janedoe");

        ITransactionDAO transactionDAO = new TransactionDAO();
        ITransactionService transactionService = new TransactionService(transactionDAO);
        Transaction newTransaction = transactionService.createTransaction(20.0f, source, destination);
        boolean executionResult = newTransaction.execute();

        Assert.assertNotNull(newTransaction.getId());
        Assert.assertEquals("rejected", newTransaction.getStatus());
        Assert.assertEquals(20.0f,newTransaction.getAmount(), 0.0f);
        Assert.assertEquals(source, newTransaction.getSource());
        Assert.assertEquals(destination, newTransaction.getDestination());
        Assert.assertFalse(executionResult);

        Assert.assertEquals(0.0f, source.getBalance(), 0.0f);
        Assert.assertEquals(0.0f, destination.getBalance(), 0.0f);
    }

    @Test
    public void invalidSourceAccountTransaction() throws TransactionException {
        IAccountDAO accountDAO = new AccountDAO();
        AccountService accountService = new AccountService(accountDAO);
        Account source = accountService.createAccount(20.0f, "johndoe");
        Account destination = accountService.createAccount(0.0f, "janedoe");

        ITransactionDAO transactionDAO = new TransactionDAO();
        ITransactionService transactionService = new TransactionService(transactionDAO);
        Transaction newTransaction = transactionService.createTransaction(20.0f, source, destination);
        source.setStatus("invalid");
        boolean executionResult = newTransaction.execute();

        Assert.assertNotNull(newTransaction.getId());
        Assert.assertEquals("rejected", newTransaction.getStatus());
        Assert.assertEquals(20.0f,newTransaction.getAmount(), 0.0f);
        Assert.assertEquals(source, newTransaction.getSource());
        Assert.assertEquals(destination, newTransaction.getDestination());
        Assert.assertFalse(executionResult);

        Assert.assertEquals(20.0f, source.getBalance(), 0.0f);
        Assert.assertEquals(0.0f, destination.getBalance(), 0.0f);
    }

    @Test
    public void invalidDestinationAccountTransaction() throws TransactionException {
        IAccountDAO accountDAO = new AccountDAO();
        AccountService accountService = new AccountService(accountDAO);
        Account source = accountService.createAccount(20.0f, "johndoe");
        Account destination = accountService.createAccount(0.0f, "janedoe");

        ITransactionDAO transactionDAO = new TransactionDAO();
        ITransactionService transactionService = new TransactionService(transactionDAO);
        Transaction newTransaction = transactionService.createTransaction(20.0f, source, destination);
        destination.setStatus("invalid");
        boolean executionResult = newTransaction.execute();

        Assert.assertNotNull(newTransaction.getId());
        Assert.assertEquals("rejected", newTransaction.getStatus());
        Assert.assertEquals(20.0f,newTransaction.getAmount(), 0.0f);
        Assert.assertEquals(source, newTransaction.getSource());
        Assert.assertEquals(destination, newTransaction.getDestination());
        Assert.assertFalse(executionResult);

        Assert.assertEquals(20.0f, source.getBalance(), 0.0f);
        Assert.assertEquals(0.0f, destination.getBalance(), 0.0f);
    }
}
