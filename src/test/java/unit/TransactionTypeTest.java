package unit;

import org.junit.Assert;
import org.junit.Test;
import types.Account;
import types.Transaction;

import java.util.UUID;

public class TransactionTypeTest {
    @Test
    public void createTransaction(){
        UUID sourceUuid = UUID.randomUUID();
        Account sourceAccount = new Account(sourceUuid, 0.0f, "johndoe");
        UUID destinationUuid = UUID.randomUUID();
        Account destinationAccount = new Account(destinationUuid, 0.0f, "janedoe");
        float amount = 2.0f;

        UUID transactionId = UUID.randomUUID();
        Transaction transaction = new Transaction(transactionId, amount, sourceAccount, destinationAccount);

        Assert.assertEquals(transactionId, transaction.getId());
        Assert.assertEquals("pending", transaction.getStatus());
        Assert.assertEquals(0.0f,transaction.getAmount(), 2.0f);
        Assert.assertEquals(sourceAccount, transaction.getSource());
        Assert.assertEquals(destinationAccount, transaction.getDestination());
    }

    @Test
    public void completedTransaction() {
        UUID sourceUuid = UUID.randomUUID();
        Account sourceAccount = new Account(sourceUuid, 5.0f, "johndoe");
        UUID destinationUuid = UUID.randomUUID();
        Account destinationAccount = new Account(destinationUuid, 0.0f, "janedoe");
        float amount = 2.0f;

        UUID transactionId = UUID.randomUUID();
        Transaction transaction = new Transaction(transactionId, amount, sourceAccount, destinationAccount);
        boolean transactionResult = transaction.execute();

        Assert.assertEquals(transactionId, transaction.getId());
        Assert.assertEquals("executed", transaction.getStatus());
        Assert.assertEquals(0.0f,transaction.getAmount(), 2.0f);
        Assert.assertEquals(sourceAccount, transaction.getSource());
        Assert.assertEquals(destinationAccount, transaction.getDestination());
        Assert.assertEquals(transactionResult, true);
    }

    @Test
    public void rejectedTransaction() {
        UUID sourceUuid = UUID.randomUUID();
        Account sourceAccount = new Account(sourceUuid, 0.0f, "johndoe");
        UUID destinationUuid = UUID.randomUUID();
        Account destinationAccount = new Account(destinationUuid, 0.0f, "janedoe");
        float amount = 2.0f;

        UUID transactionId = UUID.randomUUID();
        Transaction transaction = new Transaction(transactionId, amount, sourceAccount, destinationAccount);
        boolean transactionResult = transaction.execute();

        Assert.assertEquals(transactionId, transaction.getId());
        Assert.assertEquals("rejected", transaction.getStatus());
        Assert.assertEquals(0.0f,transaction.getAmount(), 2.0f);
        Assert.assertEquals(sourceAccount, transaction.getSource());
        Assert.assertEquals(destinationAccount, transaction.getDestination());
        Assert.assertEquals(transactionResult, false);
    }

}
